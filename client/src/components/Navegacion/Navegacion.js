import React from 'react';
import { NavLink } from 'react-router-dom';
// import './Navegacion.css';

import Header from '../Header/Header';

const Navegacion = () => {
     return ( 

          <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#"><Header /></a>
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
               <li class="nav-item active">
               <NavLink to={'/nosotros'} activeClassName="activo" className="nav-link" >Nosotros</NavLink>
               {/* <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a> */}
               </li>
               <li class="nav-item">
               <NavLink to={'/productos'} activeClassName="activo" className="nav-link">Productos</NavLink>
               {/* <a class="nav-link" href="#">Link</a> */}
               </li>
               <li class="nav-item">
               <NavLink to={'/contacto'} activeClassName="activo" className="nav-link">Contacto</NavLink>
               {/* <a class="nav-link disabled" href="#">Disabled</a> */}
               </li>
          </ul>
          </div>
          </nav>
      );
}
 
export default Navegacion;