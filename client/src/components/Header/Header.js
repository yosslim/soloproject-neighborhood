import React from 'react';
import { Link } from 'react-router-dom';

import './Header.css';

const Header = () => {
     return (
          <header>
               <Link to={'/'}>
                         <img src="/img/logo2.png" alt="logo imagen"/>
               </Link>              
          </header>
     )
}
 
export default Header;